FROM alpine:3.20.2

RUN apk add --no-cache bash

COPY ./install.sh /install.sh
RUN chmod +x /install.sh
RUN /install.sh

CMD ["/usr/local/bin/kops"]