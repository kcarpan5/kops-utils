#!/usr/bin/env sh

KOPS_VERSION=v1.29.2
TERRAFORM_VERSION=1.9.4
TERRAGRUNT_VERSION=v0.66.3
ETCD_MANAGER_CTL_VERSION=3.0.20210228
KUBECTL_VERSION=v1.30.3

HELM_VERSION=v3.15.3
HELM_SECRET_PLUGIN_VERSION=v4.6.1
HELM_DIFF_PLUGIN_VERSION=v3.9.9
HELMFILE_VERSION=0.167.1

# Install git
apk add git

# Install curl
apk add curl

# Install kops
curl -L https://github.com/kubernetes/kops/releases/download/${KOPS_VERSION}/kops-linux-amd64 -o /usr/local/bin/kops
chmod +x /usr/local/bin/kops

# Install terraform
curl -L https://releases.hashicorp.com/terraform/${TERRAFORM_VERSION}/terraform_${TERRAFORM_VERSION}_linux_amd64.zip -o /tmp/terraform.zip
unzip /tmp/terraform.zip -d /usr/local/bin
rm /tmp/terraform.zip
chmod +x /usr/local/bin/terraform

# Install terragrunt
curl -L https://github.com/gruntwork-io/terragrunt/releases/download/${TERRAGRUNT_VERSION}/terragrunt_linux_amd64 -o /usr/local/bin/terragrunt
chmod +x /usr/local/bin/terragrunt

# Install etcd-manager-ctl
curl -L https://github.com/kopeio/etcd-manager/releases/download/${ETCD_MANAGER_CTL_VERSION}/etcd-manager-ctl-linux-amd64 -o /usr/local/bin/etcd-manager-ctl
chmod +x /usr/local/bin/etcd-manager-ctl

# Install kubectl
curl -L https://storage.googleapis.com/kubernetes-release/release/${KUBECTL_VERSION}/bin/linux/amd64/kubectl -o /usr/local/bin/kubectl
chmod +x /usr/local/bin/kubectl

# Install helm
curl -L https://get.helm.sh/helm-${HELM_VERSION}-linux-amd64.tar.gz -o /tmp/helm.tar.gz
tar -xvf /tmp/helm.tar.gz linux-amd64/helm
mv linux-amd64/helm /usr/local/bin/helm
rm -rf /tmp/helm.tar.gz linux-amd64
chmod +x /usr/local/bin/helm

# Install helm-plugin-secrets
helm plugin install https://github.com/jkroepke/helm-secrets --version ${HELM_SECRET_PLUGIN_VERSION}

# Install helm-plugin-diff
helm plugin install https://github.com/databus23/helm-diff --version ${HELM_DIFF_PLUGIN_VERSION}

# Install helmfile
curl -L https://github.com/helmfile/helmfile/releases/download/v${HELMFILE_VERSION}/helmfile_${HELMFILE_VERSION}_linux_amd64.tar.gz -o /tmp/helmfile.tar.gz
tar -xvf /tmp/helmfile.tar.gz helmfile
mv helmfile /usr/local/bin/helmfile
chmod +x /usr/local/bin/helmfile

# Install AWS Cli
apk add aws-cli
